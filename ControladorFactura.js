module.exports=function(app){
	return{
		add:function(req,res){
			var pool=app.get('pool');
			pool.getConnection(function(err,connection){
				if(err){
					connection.release();
					res.json({"code" : 100, "status" : "Error al conectar a la base de datos"});
				}
				connection.query("INSERT INTO Factura VALUES (NULL,'"+req.body.idfactura+"','"+req.body.idUsuario+"','"+req.body.fechaHora+"','"+req.body.total+"');", function(err,row){
				if(err)
					throw err;
				else
					res.json({"mensaje":"Factura Agregada"});
				connection.release();
				});
			});
		},
				delete:function(req,res){
			var pool=app.get('pool');
			pool.getConnection(function(err,connection){
				if(err){
                    connection.release();
                    res.json({"code" : 100, "status" : "Error al conectar a la base de datos"});
                }
				connection.query("Delete from Factura where idfactura="+req.body.idfactura, function(err, row){
					if(err)
						throw err;
					else
						res.json({"mensaje":"Factura eliminada"});
					connection.release();	
				});
			});	
		},
		list:function(req,res){
			var pool=app.get('pool');
			pool.getConnection(function(err,connection){
				if(err){
                    connection.release();
                    res.json({"code" : 100, "status" : "Error al conectar a la base de datos"});
                }
				connection.query("Select * from Factura where idfactura="+req.query.idfactura, function(err, row){
					if(err)
						throw err;
					else
						res.json(row);
					connection.release();	
				});
			});	
		},
		edit:function(req,res){
			var pool=app.get('pool');
			pool.getConnection(function(err,connection){
				if(err){
                    connection.release();
                    res.json({"code" : 100, "status" : "Error al conectar a la base de datos"});
                }
				connection.query("UPDATE Factura set idUsuario='"+req.body.idUsuario+"',fechaHora="+req.body.fechaHora+"' where total="+req.body.total, function(err, row){
					if(err)
						throw err;
					else
						res.json({"mensaje":"Factura editada"});
					connection.release();	
				});
			});	
		}
	}
}